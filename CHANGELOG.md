# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [3.3.1.3] - 2019-12-20
### Added
- Added new `jquery-browser` web-resource to implement `jQuery.browser` object. 
- Replaced `jQuery.browser` implementation in jquery-migrate with one from [jquery-browser-plugin](https://github.com/gabceb/jquery-browser-plugin).
  It can now detect IE 11 and Edge browsers.

## [2.2.4.9] - 2019-12-20
### Added
- Added new `jquery-browser` web-resource to implement `jQuery.browser` object. 
- Replaced `jQuery.browser` implementation in jquery-migrate with one from [jquery-browser-plugin](https://github.com/gabceb/jquery-browser-plugin).
  It can now detect IE 11 and Edge browsers.

## [2.2.4.8] - 2019-08-02
### Fixed
- Apply 'proto' security fix from jQuery 3.4.0.
- Reverted removal of `jQuery.browser` from the jquery-migrate plugin.

[Unreleased]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F3.3.1.3%0DHEAD
[3.3.1.3]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F3.3.1.3%0Drelease%2F3.3.1.2
[2.2.4.9]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.9%0Drelease%2F2.2.4.8
[2.2.4.8]: https://bitbucket.org/atlassian/atlassian-plugins-jquery/compare/release%2F2.2.4.8%0Drelease%2F2.2.4.7
